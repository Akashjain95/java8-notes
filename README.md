Java8 notes for reference
->java lambda expressions

1.lambda expressions provide implementation of functional interfaces.
An interface which has only one abstract method is called functional interface. Java provides an annotation @FunctionalInterface,
which is used to declare an interface as functional interface.

2.Java Lambda Expression Syntax
(argument-list) -> {body}
1) Argument-list: It can be empty or non-empty as well.

2) Arrow-token: It is used to link arguments-list and body of expression.

3) Body: It contains expressions and statements for lambda expression

3.Example:
@FunctionalInterface
interface Student
{
void fun();
}

public class bar
{
    public static void main(String[] args)
    {
     Student s=()->{System.out.print("Hello");};
     s.fun();
    }
};

->java stream
1.Stream does not store elements. It simply conveys elements from a source such as a data structure,
an array, or an I/O channel, through a pipeline of computational operations.

2.Filter working:
List<Product> productsList = new ArrayList<Product>();
        //Adding Products
        productsList.add(new Product(1,"HP Laptop",25000f));
        productsList.add(new Product(2,"Dell Laptop",30000f));
        productsList.add(new Product(3,"Lenovo Laptop",28000f));
        productsList.add(new Product(4,"Sony Laptop",28000f));
        productsList.add(new Product(5,"Apple Laptop",90000f));
        // This is more compact approach for filtering data
        productsList.stream()
                .filter(product -> {if(product.price == 30000)return true;return false;})
                .forEach(product -> System.out.println(product.name));
  3.Convert list to map
  Map<Integer,String> productPriceMap =productsList.stream().collect(Collectors.toMap(p->p.id, p->p.name));

  4.use map to get only a particular field

  Set<Float> productPriceList =
            productsList.stream()
            .filter(product->product.price < 30000)   // filter product on the base of price
            .map(product->product.price)
            .collect(Collectors.toSet());   // collect it as Set(remove duplicate elements)
        System.out.println(productPriceList);

5.using forEach with stream
productsList.stream().filter(p ->p.price> 30000)   // filtering price
                    .map(pm ->pm.price)          // fetching price
                    .forEach(System.out::println);  // iterating price
How everything depends on LAMBDA function:
ArrayList<Product> p=new ArrayList<Product>();
    p.add(new Product(1,"Dell",200.0f));
    p.add(new Product(2,"Lenovo",240.0f));
    p.add(new Product(3,"HP",260.0f));
    List<Product>new_list=p.stream().filter((temp)->{return temp.price>=230;}).collect(Collectors.toList());
    new_list.stream().forEach(s->{
        System.out.println(s.name);
    });
    Map<Integer,String> h;
    p.stream().collect(Collectors.toMap(s->{return s.id;},s->{return s.name;}))
            .forEach((a,b)-> System.out.println(b));
